<?php get_header(); ?>

<section class="section-news-inner container">
  <div class="section-container">
    <div class="section-container__inner">
      <?php if (have_posts()): ?> 
      <?php while (have_posts()): the_post(); ?>

      <?php the_content(); ?>

      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php $news = function_exists('icl_object_id') ? get_page(icl_object_id(get_page_by_path("news")->ID, 'page', false)) : get_page_by_path("news"); ?>

<?php if ($news): ?>
<section class="section-news section-news--inner container">
  <div class="section-container">
    <div class="section-news__news-title">
      <div class="section-news__news-title__left">
        <h2 class="text-blue"><?= $news->post_title ?></h2>
      </div>
      <div class="section-news__news-title__right">
        <p class="text-blue"><?= $news->post_excerpt ?></p>
      </div>
    </div>
    <div class="news-list">
      <a class="news-list__show-all" href="<?= get_permalink(get_page_by_path('news')); ?>"><?= esc_attr__('Show all News', 'spectrum'); ?></a>
      <?php
        $args = array(
            'post_type' => 'post',
            'posts_per_page' => -1
        );

        $post_query = new WP_Query($args);

        if($post_query->have_posts()): $i = 0; ?>
          <?php while($post_query->have_posts()): $i++; $post_query->the_post(); ?>

          <?php if ($i === 2): ?>
          <div class="news-slider">
          <?php endif; ?>

          <?php get_template_part( 'template-parts/post' ); ?>
          <?php endwhile; wp_reset_postdata(); ?>
        <?php endif; ?>

      <?php if ($i > 1): ?>
      </div>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php endif; ?>

<?php get_footer(); ?>