<?php get_header(); ?>

<section class="section-news-inner container">
  <div class="section-container">
    <div class="section-container__inner">
      <?php if (have_posts()): ?> 
      <?php while (have_posts()): the_post(); ?>

      <?php the_content(); ?>

      <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
</section>

<?php get_footer(); ?>