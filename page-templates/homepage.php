<?php
/**
 * Template Name: Homepage
 */
get_header();

$dir = get_theme_file_uri();
?>

<div class="main-header" style="background-image: url('<?= get_the_post_thumbnail_url(null,'full') ?>')">
  <div class="container">
    <div class="main-header__nav">
      <a href="">
        <img src="<?= $dir ?>/static/images/logo.png" alt="logo">
      </a>

      <div class="main-header__nav__links">
        <a href="tel:<?= get_field('contact_info')['phone_number']; ?>"><img src="<?= $dir ?>/static/images/icons/phone.svg" alt="phone"><span><?= get_field('contact_info')['phone_number']; ?></span></a>
        <a href="mailto:<?= get_field('contact_info')['email']; ?>"><img src="<?= $dir ?>/static/images/icons/email.svg" alt="email"><span><?= get_field('contact_info')['email']; ?></span></a>
      </div>
    </div>

    <h1><?= get_field('title'); ?></h1>
    <p><?= get_field('subtitle'); ?></p>
  </div>
</div>

<div class="container">
  <div class="calculator">
    <div class="calculator__car">
      <p><?= get_field('calculator')['car_from']; ?></p>
      <multiselect v-model="type" :options="typeOptions" :show-labels="false" placeholder="Pasirinkite" track-by="value" label="name"></multiselect>
    </div>

    <div class="calculator__period">
      <p><?= get_field('calculator')['contract_period']; ?></p>
      <multiselect v-model="period" :options="periodOptions" :show-labels="false" placeholder="Pasirinkite" track-by="value" label="name"></multiselect>
    </div>

    <div class="calculator__price">
      <p><?= get_field('calculator')['price']; ?></p>
      <input class="input" type="text" placeholder="EUR" v-model="price" maxlength="6" @input="updateValue">
    </div>

    <div class="calculator__get-price">
      <button class="button-primary" @click.prevent="calculate()" :disabled="!allDataFilled"><?= get_field('calculator')['calculate']; ?></button>
    </div>
  </div>
  <div class="calculated-answer__link">
    <a class="calc" target="_blank" href="<?= get_field('calculator')['our_cars_link']; ?>"><?= get_field('calculator')['our_cars']; ?></a>
    <a target="_blank" href="<?= get_field('calculator')['our_cars_link']; ?>" class="button-primary"><?= get_field('calculator')['here']; ?></a>
  </div>
  <div class="calculated-answer" v-if="calculated">
    <div class="calculated-answer__inner">
      <div class="calculated-answer__inner-block">
        <p><?= get_field('calculator')['initial_contribution']; ?></p>
        <h3>{{ getInitialContributionNice }} EUR*</h3>
      </div>

      <div class="calculated-answer__inner-block">
        <p><?= get_field('calculator')['preliminary_monthly_payment']; ?></p>
        <h3>{{ getMonthlyPayment }} EUR*</h3>
      </div>

      <div class="calculated-answer__inner-block">
        <button class="button-primary" @click.prevent="getQuery()"><?= get_field('calculator')['make_a_request']; ?></button>
      </div>
    </div>
  </div>

  <div class="benefits">
    <h2><?= get_field('why_easycar')['title']; ?></h2>
    
    <div class="benefits__blocks">
      <div class="benefits__blocks__benefit">
        <div class="benefits__blocks__benefit-inner">
          <img src="<?= $dir ?>/static/images/icons/fast.svg" alt="">
          <h3><?= get_field('why_easycar')['first']; ?></h3>
        </div>
      </div>

      <div class="benefits__blocks__benefit">
        <div class="benefits__blocks__benefit-inner">
          <img src="<?= $dir ?>/static/images/icons/flexible.svg" alt="">
          <h3><?= get_field('why_easycar')['second']; ?></h3>
        </div>
      </div>

      <div class="benefits__blocks__benefit">
        <div class="benefits__blocks__benefit-inner">
          <img src="<?= $dir ?>/static/images/icons/money.svg" alt="">
          <h3><?= get_field('why_easycar')['third']; ?></h3>
        </div>
      </div>

      <div class="benefits__blocks__benefit">
        <div class="benefits__blocks__benefit-inner">
          <img src="<?= $dir ?>/static/images/icons/customer-support.svg" alt="">
          <h3><?= get_field('why_easycar')['fourth']; ?></h3>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="contacts-footer">
  <div class="container">
    <div class="contacts-footer__columns" ref="contactForm">
      <div class="contacts-footer__form">
        <div class="input-wrap">
          <input v-model="form.name" class="input contact-form" type="text" placeholder="Vardas Pavardė ar įmonės pavadinimas">
          <img src="<?= $dir ?>/static/images/icons/cf-name.svg" alt="">
        </div>
        <div class="input-wrap">
          <input v-model="form.phone" class="input contact-form" type="text" placeholder="Tel. numeris (privaloma)">
          <img src="<?= $dir ?>/static/images/icons/cf-phone.svg" alt="">
        </div>
        <div class="input-wrap">
          <input v-model="form.email" class="input contact-form" type="text" placeholder="El. pašto adresas">
          <img src="<?= $dir ?>/static/images/icons/cf-email.svg" alt="">
        </div>
        <div class="input-wrap">
          <textarea v-model="form.comment"  class="textarea contact-form" placeholder="Komentaras"></textarea>
          <img src="<?= $dir ?>/static/images/icons/cf-comment.svg" alt="">
        </div>

        <button :disabled="!allowSendForm" class="button-primary" @click.prevent="sendForm()">Patvirtinti užklausą</button>
      </div>
      <div class="contacts-footer__contact">
        <h2><?= get_field('contact_info')['request_quote_text']; ?></h2>

        <div class="contacts-footer__contact__details">
          <a href="tel:<?= get_field('contact_info')['phone_number']; ?>"><img src="<?= $dir ?>/static/images/icons/phone.svg" alt="phone"><span><?= get_field('contact_info')['phone_number']; ?></span></a>
          <span></span>
          <a href="mailto:<?= get_field('contact_info')['email']; ?>"><img src="<?= $dir ?>/static/images/icons/email.svg" alt="phone"><span><?= get_field('contact_info')['email']; ?></span></a>
        </div>
      </div>
    </div>

    <p v-if="formSent" class="form-sent">Dėkui už užklausą, netrukus su Jumis susisieksime</p>

    <div class="contacts-footer__info">
      <p><?= get_field('footer_notice'); ?></p>
      <p><?= get_field('footer_right'); ?></p>
    </div>
  </div>
</div>

<?php get_footer(); ?>