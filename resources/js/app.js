import '@babel/polyfill';
import axios from 'axios'
import VueAxios from 'vue-axios'
import Multiselect from 'vue-multiselect'

Vue.config.devtools = true;

Vue.use(VueAxios, axios);

new Vue({
  el: document.getElementById('app'),
  data: {
    typeOptions: [
      {
        name: 'Iš mūsų autoparko',
        value: 1
      },
      {
        name: 'Iš kito autoparko',
        value: 2
      }
    ],
    interest: 0.468,
    type: null,
    period: null,
    periodOptions: [
      {
        name: "12 mėn",
        value: 12
      },
      {
        name: "24 mėn",
        value: 24
      },
      {
        name: "36 mėn",
        value: 36
      }
    ],
    price: '',
    percent: {
      1: {
        12: 15,
        24: 15,
        36: 20
      },
      2: {
        12: 25,
        24: 25,
        36: 30
      }
    },
    form: {
      name: '',
      phone: '',
      email: '',
      comment: ''
    },
    calculated: false,
    formSent: false,
  },
  mounted() {
  },
  watch: {
  },
  computed: {
    allowSendForm() {
      if (this.formSent) {
        return false;
      }

      let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      if (this.form.email && !re.test(String(this.form.email).toLowerCase())) {
        return false;
      }

      if (this.form.phone.length < 8) {
        return false;
      }
      
      return true;
    },
    getMonthlyPayment() {
      if (!this.period || !this.type || !this.getInitialContribution) {
        return '??';
      }

      if (this.period.value === 12) {
         return parseInt(((this.getAmountFinanced * this.interest) * 0.58 + this.getAmountFinanced) / 12);
      } else if (this.period.value === 24) {
        return parseInt(((this.getAmountFinanced * this.interest) * 0.58 + (this.getAmountFinanced * this.interest) * 0.61 + this.getAmountFinanced) / 24);
      } else if (this.period.value === 36) {
        return parseInt(((this.getAmountFinanced * this.interest) * 0.92 + (this.getAmountFinanced * this.interest) * 0.68 + (this.getAmountFinanced * this.interest) * 0.28 + this.getAmountFinanced) / 36);
      }
    },
    getAmountFinanced() {
      return this.price - this.getInitialContribution;
    },
    getInitialContribution() {
      if (!this.period || !this.type) {
        return '??';
      }

      return this.price * this.percent[this.type.value][this.period.value] / 100;
    },
    getInitialContributionNice() {
      if (this.getInitialContribution) {
        return parseInt(this.getInitialContribution);
      }

      return '??';
    },
    allDataFilled() {
      if (!Number.isInteger(this.price)) {
        return false;
      }

      if (!this.period || !this.type) {
        return false;
      }

      return true;
    }
  },
  methods: {
    getFormData() {
      return `Iš kur auto: ${this.type.name}. Sutarties laikotarpis: ${this.period.name}. Turto kaina: ${this.price}. Pradinio įnašo suma: ${this.getInitialContributionNice} EUR. Preliminari mėnesio įmoka: ${this.getMonthlyPayment} EUR`; 
    },
    sendForm() {
      this.formSent = true;
      var bodyFormData = new FormData();

      bodyFormData.append('your-name', this.form.name);
      bodyFormData.append('your-phone', this.form.phone);
      bodyFormData.append('your-email', this.form.email);
      bodyFormData.append('your-comment', this.form.comment);
      if (this.calculated) {
        bodyFormData.append('pasiulymas', this.getFormData());
      }

      axios({
        method: "post",
        url: "/wp-json/contact-form-7/v1/contact-forms/8/feedback",
        data: bodyFormData,
        headers: { "Content-Type": "multipart/form-data" }
      })
      .then(function(response) {
        this.formSent = true;
      }.bind(this))
      .catch(function(response) {
      });
    },
    getQuery() {
      const el = this.$refs.contactForm;

      if (el) {
        el.scrollIntoView({behavior: 'smooth'});
      }
    },
    calculate() {
      this.calculated = true;
    },
    updateValue(e) {
      let val = e.target.value.replace(/[^0-9]+/g, '');

      if (val) {
        this.price = parseInt(val);
      }
    }
  },
  mounted() {
  },
  components: {
    Multiselect
  }
});
