<?php

function tk_init_blocks() {
  // Check if function exists and hook into setup.
  if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
  }
}

function register_acf_block_types() {
  // register a testimonial block.
  acf_register_block_type(array(
      'name'              => 'post-navigation',
      'title'             => __('Post navigation'),
      'description'       => __('A custom post navigation.'),
      'render_template'   => 'template-parts/blocks/post-navigation/post-navigation.php',
      'category'          => 'formatting',
      'icon'              => 'editor-ul',
      'keywords'          => array( 'navigation'),
  ));

  acf_register_block_type(array(
    'name'              => 'photo-text',
    'title'             => __('Photo text'),
    'description'       => __('A custom photo text block.'),
    'render_template'   => 'template-parts/blocks/photo-text/photo-text.php',
    'category'          => 'formatting',
    'icon'              => 'format-image',
    'keywords'          => array( 'photo', 'text'),
  ));

  acf_register_block_type(array(
    'name'              => 'statistics',
    'title'             => __('Statistics'),
    'description'       => __('A custom statistics block.'),
    'render_template'   => 'template-parts/blocks/statistics/statistics.php',
    'category'          => 'formatting',
    'icon'              => 'megaphone',
    'keywords'          => array( 'statistics'),
  ));
}