<?php

function tk_register_custom_post_types() {
    register_post_type( 'services',
        array(
            'labels' => array(
                'name' => __( 'Services', 'spectrum' ),
                'singular_name' => __( 'Service', 'spectrum' )
            ),
            'public' => true,
            'has_archive' => true,
            'supports' => array('thumbnail', 'editor', 'title', 'excerpt'),
            'rewrite' => array('slug' => 'services', 'with_front' => false),
            'show_in_rest' => true,
        )
    );
}

function set_posts_per_page_for_services_cpt( $query ) {
  if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'services' ) ) {
    $query->set( 'posts_per_page', '-1' );
  }
}

add_action( 'pre_get_posts', 'set_posts_per_page_for_services_cpt' );