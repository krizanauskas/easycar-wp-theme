<?php

function tk_enqueue() {
  $version = TK_DEV_MODE === true ? time() : '2';
  $uri = get_theme_file_uri();

  wp_register_style('tk_main', $uri . '/static/css/app.css', [], $version);
  wp_register_style('tk_vue-multiselect', $uri . '/static/css/vue-multiselect.min.css');

  // wp_register_style('tk_tiny_slider', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css', [], $version);

  // wp_enqueue_style('tk_tiny_slider');
  wp_enqueue_style('tk_main');
  wp_enqueue_style('tk_vue-multiselect');
  
  // wp_register_script('tk_main_js', $uri . '/static/js/app.js', [], $version, true);
  // wp_register_script('tk_tiny_slider', 'https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/min/tiny-slider.js', [], false, true);
  // wp_register_script('tk_jquery_ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', [], false, true);

  wp_register_script('tk_vue', $uri . '/static/js/vue.js', [], $version, true);
  wp_register_script('tk_app', $uri . '/static/js/app.js', [], $version, true);

  wp_enqueue_script('jquery');
  // wp_enqueue_script('tk_jquery_ui');
  // wp_enqueue_script('tk_tiny_slider');

  wp_enqueue_script('tk_vue');
  wp_enqueue_script('tk_app');
}