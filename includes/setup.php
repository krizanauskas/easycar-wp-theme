<?php

function tk_setup_theme() {
  add_theme_support('post-thumbnails');
  add_theme_support('title-tag');

  register_nav_menu('primary', __('Primary Menu', 'spectrum'));

  add_filter('wpcf7_autop_or_not', '__return_false');
  add_post_type_support( 'post', 'excerpt' );
  add_post_type_support( 'page', 'excerpt' );
  remove_filter( 'the_excerpt', 'wpautop' );

  // if( function_exists('acf_add_options_page') ) {
  //   acf_add_options_page();
  //   acf_add_options_sub_page(array('page_title' => __('Footer', 'spectrum'), 'post_id' => 'footer_' . ICL_LANGUAGE_CODE));
  //   acf_add_options_sub_page(array('page_title' => __('Header', 'spectrum'), 'post_id' => 'header'));
  // }

  // acf_update_setting('google_api_key', GOOGLE_MAPS_API_KEY);
}